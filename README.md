# What is Cryptogrphr
Cryptogrphr is a command-line tool to standardize the configuration settings for signatures/attestations allowing teams using hoppr, droppr, and other tools to share a common configuration across them.  This helps simplify maintenance, eliminate duplication, and ensure a stable flow of signature validation across their environments.  

Cryptogrphr simplifies CA and Key management across the Hoppr Ecosystem.

# Why
This utility is intended to support teams that require environment unique certificate authorities, specific key management systems, specific key usage, and much more.  Air-gapped systems do not have connectivity or consistent certificate authority solutions.  Instead, each unique air-gap has a reponsible organization establishing a Certificate Authority, Key Management Systems, Unique Crypto Requirements, and much more based upon the particular constraints and intent of the system.  This can be anything from a single customized hardware devices to data center systems, so, the architecture and implementation varies to support it's unique challenges.

Air gapped systems need to pull in content / applications from suppliers or vendors often from a variety of sources.  This utility helps to condifiy configuration settings across both sides of the air-gap to keep them synchornized and compatible.  Without this ability transfers across the air gap could not be validated reliably breaking the chain of trust.  This utility allows teams to share the configuration settings rather than manually edit and maintain them indivdiually across tools and various deployments/instances.

This utility is written in GO to support Droppr's primary architecture of relying on a single executable to simplify installation and usage.

# Getting Started

## Installation

Go to Releases and download the appropriate binary for your system.

## Usage

See Using Cryptogrphr for more information.

## Join Us
We're completely open source, [MIT licensed](https://gitlab.com/hoppr/droppr/-/blob/dev/LICENSE), and we welcome community contributions!